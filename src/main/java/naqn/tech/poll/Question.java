package naqn.tech.poll;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "question_generator")
    @SequenceGenerator(name="question_generator", sequenceName = "question_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)       
	private Long id;		
	@NotBlank(message = "Question is mandatory")
    private String que;
	@NotBlank(message = "A option is mandatory")	
    private String a;    
	@NotBlank(message = "B option is mandatory")	
    private String b;
	@NotBlank(message = "C option is mandatory")	
    private String c;
	@NotBlank(message = "D option is mandatory")	
    private String d;	
	@NotBlank(message = "answer option is mandatory")	
    private String answer;
	//@NotBlank(message = "time is mandatory")	
    private Long time = Long.MAX_VALUE;
	
	public Question() {		
	}
	
	public Question(String question, String a,  String b, String c,  String d, String answer) {
		super();
		this.que = question;
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;		
		this.answer = answer;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getQue() {
		return que;
	}
	public void setQue(String question) {
		this.que = question;
	}
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getB() {
		return b;
	}
	public void setB(String b) {
		this.b = b;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public String getD() {
		return d;
	}
	public void setD(String d) {
		this.d = d;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
		
}
