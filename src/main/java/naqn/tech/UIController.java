package naqn.tech;

import naqn.tech.poll.Issue;
import naqn.tech.poll.Quiz;
import naqn.tech.poll.QuizService;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UIController {

	@Autowired
	private QuizService quizService; 

    // ####  I S S U E - related
	
    @RequestMapping(path = {"/issues/{idQuiz}"})
    public String questionsFromQuiz(Model model, @PathVariable("idQuiz") Long idQuiz) {
    	List<Issue> iList = quizService.getQuizIssues(idQuiz);    
    	Quiz quiz = quizService.getQuiz(idQuiz);
        model.addAttribute("issues", iList);
        model.addAttribute("idQuiz", idQuiz);
        model.addAttribute("quizName", quiz.getName());
        return "list-issues";    	
    }

    @RequestMapping(path = {"/delIssue/{id}"})
    public String delIssue(Model model, @PathVariable("id") Long id) {
    	Issue issue = quizService.getIssueById(id); 
    	quizService.delIssue(id);
        return "redirect:/issues/"+issue.getFkIdQuiz();
    }        
    
    @RequestMapping(path = {"/newIssue/{idQuiz}"})
    public String newIssue(Model model, @PathVariable("idQuiz") Long idQuiz) {   	
    	Issue tmpIssue = new Issue();
    	tmpIssue.setFkIdQuiz(idQuiz);
        model.addAttribute("issue", tmpIssue);
        return "add-issue";
    }

    @RequestMapping(path = "/addIssue", method = RequestMethod.POST)
    public String addIssue(Issue issue) {
    	quizService.addQuizIssue(issue);
        return "redirect:/issues/"+issue.getFkIdQuiz();
    }    

    @RequestMapping(path = "/saveIssue", method = RequestMethod.POST)
    public String saveIssue(Issue issue) {
    	quizService.saveIssue(issue);
        return "redirect:/issues/"+issue.getFkIdQuiz();
    }    
    
    @RequestMapping(path = {"/editIssue/{id}"})
    public String editIssue(Model model, @PathVariable("id") Long id) {        	
        Issue issue = quizService.getIssueById(id);            
        model.addAttribute("issue", issue);
        return "edit-issue";
    }
    
    // ####  Q U I Z - related         

    @RequestMapping(value = "/quizes", method = RequestMethod.GET)
    public String getQuizes(Model model) {
        List<Quiz> qlist = quizService.getAllQuizes(); 
        model.addAttribute("quizes", qlist);
        return "list-quizes";
    }

    @RequestMapping(path = {"/editQuiz", "/editQuiz/{id}"})
    public String editQuizById(Model model, @PathVariable("id") Optional<Long> id) {
        if (id.isPresent()) {        	
            Quiz quiz = quizService.getQuiz(id.get());            
            model.addAttribute("quiz", quiz);
        } else {
            model.addAttribute("quiz", new Quiz());
        }
        return "add-edit-quiz";
    }

    @RequestMapping(path = {"/delQuiz", "/delQuiz/{id}"})
    public String delQuizById(Model model, @PathVariable("id") Long id) {
    	quizService.delQuiz(id);
        return "redirect:/quizes";
    }    
    
    @RequestMapping(path = "/saveQuiz", method = RequestMethod.POST)
    public String createOrUpdateQuiz(Quiz quiz) {
    	quizService.addQuiz(quiz);
        return "redirect:/quizes";
    }
    
}
 