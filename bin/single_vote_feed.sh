#!/bin/bash

REMOTE_TARGET=$1
ANSWER=$2

if [ -z "$REMOTE_TARGET" ]; then 
  echo "Missing URL (http://ip:port)"
  echo "script URL Answer"
  exit 1 
fi

if [ -z "$ANSWER" ]; then 
  echo "Missing Answer (A,B,C,D)"
  exit 2
fi


TIME=$(date +%s%N | cut -b1-13)
curl $REMOTE_TARGET/votes -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"answer":"'$ANSWER'","device":"linux_robot","time":"'$TIME'"}'
