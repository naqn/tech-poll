package naqn.tech.poll;

import javax.persistence.*;

@Entity
@Table(name = "vote")
@IdClass(VotePK.class)
public class Vote {
    @Id
	private Long id;
    @Id    
	private Long fkIdQuestion;	
	private String answer; 
	private String identUser; // on each onCreate (VoteFormActivity)
	private String identSession; // on each app start (static in MainActivity)
	private String device;  // device unique id (//@TODO md5sum)	
	private Long time; // when answer was prepared for post
	
	public Vote() {
	}
	
	public Vote(String answer) {
		super();
		this.answer = answer;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFkIdQuestion() {
		return fkIdQuestion;
	}
	public void setFkIdQuestion(Long fkIdQuestion) {
		this.fkIdQuestion = fkIdQuestion;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getIdentUser() {
		return identUser;
	}
	public void setIdentUser(String identUser) {
		this.identUser = identUser;
	}
	public String getIdentSession() {
		return identSession;
	}
	public void setIdentSession(String identSession) {
		this.identSession = identSession;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Vote [id=" + id + ", fkIdQuestion=" + fkIdQuestion + ", answer=" + answer + ", identUser=" + identUser
				+ ", identSession=" + identSession + ", device=" + device + ", time=" + time + "]";
	}
	
}

