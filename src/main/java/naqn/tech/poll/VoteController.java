package naqn.tech.poll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VoteController {

	@Autowired
	private VoteService voteService; 
	
	@RequestMapping("/ques")
	public List<Question> getAllQuestions() {
		return voteService.getAllQuestions();
	}

	@RequestMapping("/que")
	public Question getCurrentQuestion() {
		return voteService.getCurrentQuestion();
	}	
	
	@RequestMapping(method=RequestMethod.POST, value="/votes")
	public Vote addVote(@RequestBody Vote vote) {
		return voteService.addVote(vote);
	}
	
}
