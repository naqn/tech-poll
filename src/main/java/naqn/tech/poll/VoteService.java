package naqn.tech.poll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import naqn.tech.exception.RestNotFoundException;

@Service
public class VoteService {

	@Autowired VoteRepository voteRepo;
	@Autowired QuestionRepository questionRepo;
	
	public List<Question> getAllQuestions() {
//		List<Question> questionList = new ArrayList<>();
//		questionRepo.findAllQuestionsSortedByIdDesc().forEach(questionList::add);
		return questionRepo.findAllQuestionsSortedByIdDesc();
	}

	public Question getQuestion(Long id) {
		return questionRepo.findById(id);
	}	

	public Question getCurrentQuestion() {
		return questionRepo.findByTime(Long.MAX_VALUE);
	}		
	
	public List<Vote> getAllVotesForQuestion(Long fkIdQuestion) {
		return voteRepo.findVotesForQuestion(fkIdQuestion);
	}	
	
	@Transactional
	public Question addQuestion(Question question) {
		// update existing top question
		Question fromTop =  questionRepo.findByTime(Long.MAX_VALUE);
		if (null != fromTop) {
			fromTop.setTime(System.currentTimeMillis());
			questionRepo.save(fromTop);			
		}
		// add on top		
		return questionRepo.save(question);
	}

	@Transactional
	public Question saveQuestion(Question question) {		
		return questionRepo.save(question);
	}	
	
	@Transactional
	public Vote addVote(Vote vote) {
		Question fromTop =  questionRepo.findByTime(Long.MAX_VALUE);
		if (null == fromTop) {
			new RestNotFoundException("Found no valid question (for voting).");
		}
		Long idOfNextVote =  voteRepo.getCountOfVotesForQuestion(fromTop.getId());		
		vote.setId(++idOfNextVote);
		vote.setFkIdQuestion(fromTop.getId());		
		return voteRepo.save(vote);
	}
	
}
