package naqn.tech;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import naqn.tech.poll.Question;
import naqn.tech.poll.Vote;
import naqn.tech.poll.VoteService;

@Controller
public class GUIController {

	@Autowired
	private VoteService voteService; 
	    	
    // ####  Q U E S T I O N - related
	
    @RequestMapping(value = "/questions", method = RequestMethod.GET)
    public String getQuestions(Model model) {
        List<Question> qlist = voteService.getAllQuestions(); 
        model.addAttribute("questions", qlist);
        return "list-questions";
    }

	@RequestMapping(path = {"/newQuestion"})
	public String newQuestion(Model model) {   	
		Question tmpQuestion = new Question();
		model.addAttribute("question", tmpQuestion);
		return "add-question";
	}

	@RequestMapping(path = "/addQuestion", method = RequestMethod.POST)
	public String addQuestion(Question question) {
		voteService.addQuestion(question);			
		return "redirect:/questions";
	}    
	
	@RequestMapping(path = {"/editQuestion/{idQuestion}"})
	public String editQuestion(Model model, @PathVariable("idQuestion") Long idQuestion) {
		Question question = voteService.getQuestion(idQuestion);
		model.addAttribute("question", question);		
		return "edit-question";
	}    		
    
    @RequestMapping(path = "/saveQuestion", method = RequestMethod.POST)
    public String saveIssue(Question question) {
    	voteService.saveQuestion(question);
        return "redirect:/questions";
    }    

	
	// ####  V O T E - related
	
	@RequestMapping(path = {"/voting/{fkIdQuestion}"})
	public String votesForQuestion(Model model, @PathVariable("fkIdQuestion") Long fkIdQuestion) {
		Question question = voteService.getQuestion(fkIdQuestion);  
		List<Vote> vList = voteService.getAllVotesForQuestion(fkIdQuestion);
		model.addAttribute("question", question);
		model.addAttribute("votes", vList);
		Long acount = vList.stream().filter(x -> "A".equals(x.getAnswer())).count();
		Long bcount = vList.stream().filter(x -> "B".equals(x.getAnswer())).count();
		Long ccount = vList.stream().filter(x -> "C".equals(x.getAnswer())).count();
		Long dcount = vList.stream().filter(x -> "D".equals(x.getAnswer())).count();
		model.addAttribute("acount", acount);		
		model.addAttribute("bcount", bcount);		
		model.addAttribute("ccount", ccount);		
		model.addAttribute("dcount", dcount);
		model.addAttribute("totalcount", vList.size());
		if (vList.size() > 0) {
			model.addAttribute("apercentage", (long)((acount/(float)vList.size())*100));		
			model.addAttribute("bpercentage", (long)((bcount/(float)vList.size())*100));		
			model.addAttribute("cpercentage", (long)(ccount/(float)vList.size()*100));		
			model.addAttribute("dpercentage", (long)(dcount/(float)vList.size()*100));			
		}
		return "list-votes";    	
	}
	
}
 