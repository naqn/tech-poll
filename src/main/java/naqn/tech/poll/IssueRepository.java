package naqn.tech.poll;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface IssueRepository extends CrudRepository<Issue, String> {

	List<Issue> findById(Long fkIdQuiz);	
	//Issue findByFkIdQuizAndIdIssue(Long fkIdQuiz, Long idIssue);
	boolean existsById(Long id);	

}
