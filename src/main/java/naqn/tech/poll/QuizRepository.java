package naqn.tech.poll;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface QuizRepository extends CrudRepository<Quiz, String> {

	Quiz findById(Long idQuiz);
	Quiz deleteById(Long idQuiz);
	boolean existsById(Long idQuiz);	
	// @TODO move to issueRepo
	@Query(value = "SELECT i FROM Issue i WHERE i.fkIdQuiz = :fkIdQuiz")	
	List<Issue> findQuizIssues(@Param("fkIdQuiz") Long fkIdQuiz);
	// @TODO move to issueRepo
	@Query(value = "SELECT i FROM Issue i WHERE i.fkIdQuiz = :fkIdQuiz AND i.idIssue = :issueId")	
	Issue findIssueOfQuiz(@Param("fkIdQuiz") Long fkIdQuiz, @Param("issueId") Long issueId);	
	// @TODO move to issueRepo
	@Query(value = "SELECT i FROM Issue i WHERE i.id = :id")	
	Issue findIssueById(@Param("id") Long id);		
	
	@Query(value = "SELECT count(*) FROM Issue i WHERE i.fkIdQuiz = :fkIdQuiz")	
	Long getCountOfIssuesInQuiz(@Param("fkIdQuiz") Long fkIdQuiz);

	@Query(value = "SELECT count(*) FROM Answer a WHERE a.fkIdIssue = :fkIdIssue AND a.isFinal IS true")	
	Long getCountFinalAnswers(@Param("fkIdIssue") Long fkIdIssue);
	
}
