package naqn.tech.poll;

import javax.persistence.*;

@Entity
@Table(name = "quiz")
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "quiz_generator")
    @SequenceGenerator(name="quiz_generator", sequenceName = "quiz_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)       
	private Long id;
	private String name;
	private String time;
    @Column(nullable = false)	       
	private Boolean isActive = false;	

	public Quiz() {
		this.time = ""+System.currentTimeMillis();
		this.isActive = false;
	}
	
	public Quiz(String name, String time) {
		super();
		this.name = name;
		this.time = time;
		this.isActive = false;		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
}
