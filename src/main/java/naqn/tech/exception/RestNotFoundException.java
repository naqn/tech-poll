package naqn.tech.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RestNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -730885776301429040L;

	public RestNotFoundException(String string) {
		super(string);
	}

}
