package naqn.tech.poll;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<Question, String> {

	Question findById(Long id);
	Question findByTime(Long time);
	
	@Query(value = "SELECT q FROM Question q order by q.id desc")	
	List<Question> findAllQuestionsSortedByIdDesc();
	
}
