package naqn.tech.poll;

import javax.persistence.*;

@Entity
@Table(name = "answer")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "answer_generator")
    @SequenceGenerator(name="answer_generator", sequenceName = "answer_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)       
	private Long id;
	private Long fkIdIssue;	
	private String identUser; // on each onCreate (QuizFormActivity)
	private String identSession; // on each app start (static in MainActivity)
    private String answer;
	private String device;	
	private String time;
    @Column(nullable = false)	       
	private Boolean isFinal = false;
	
	public Answer() {
	}
	
	public Answer(String answer) {
		super();
		this.answer = answer;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFkIdIssue() {
		return fkIdIssue;
	}
	public void setFkIdIssue(Long fkIdssue) {
		this.fkIdIssue = fkIdssue;
	}
	public String getIdentUser() {
		return identUser;
	}
	public void setIdentUser(String identUser) {
		this.identUser = identUser;
	}
	public String getIdentSession() {
		return identSession;
	}
	public void setIdentSession(String identSession) {
		this.identSession = identSession;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Boolean getIsFinal() {
		return isFinal;
	}
	public void setIsFinal(Boolean isFinal) {
		this.isFinal = isFinal;
	}

	@Override
	public String toString() {
		return "Answer [id=" + id + ", fkIdIssue=" + fkIdIssue + ", identUser=" + identUser + ", identSession="
				+ identSession + ", answer=" + answer + ", device=" + device + ", time=" + time + ", isFinal=" + isFinal
				+ "]";
	}
	
}

