package naqn.tech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PollApiApp {

	public static void main(String[] args) {		
		SpringApplication.run(PollApiApp.class, args);
	}

}
