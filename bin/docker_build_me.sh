# build spring jar locally
mvn clean package
# build local docker image
docker build -t=techpoll .
# manage containers (raise fresh one eventually)
docker stop poll
docker rm poll
docker run -d --name poll -p 7788:8877 techpoll 
# docker run -it --name poll -p 7788:8877 techpoll "/bin/bash"
