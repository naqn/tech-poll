package naqn.tech.poll;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import naqn.tech.exception.RestConflictException;

@Service
public class QuizService {

	@Autowired QuizRepository quizRepo;
	@Autowired IssueRepository issueRepo;
	@Autowired AnswerRepository answerRepo;
	
	public List<Quiz> getAllQuizes() {
		List<Quiz> quizList = new ArrayList<>();
		quizRepo.findAll().forEach(quizList::add);
		return quizList;
	}
	
	public Quiz getQuiz(Long id) {
		return quizRepo.findById(id);		
	}
	
	@Transactional
	public void delQuiz(Long id) {
		Quiz quiz = quizRepo.findById(id);		
		quizRepo.delete(quiz);				
	}	

	@Transactional
	public void delIssue(Long id) {
		Issue issue = quizRepo.findIssueById(id);		
		issueRepo.delete(issue);				
	}	
	
	public List<Issue> getQuizIssues(Long quizId) {
		return quizRepo.findQuizIssues(quizId);		
	}

	public Issue getIssueOfQuiz(Long quizId, Long issueId) {
		return quizRepo.findIssueOfQuiz(quizId, issueId);		
	}

	public Issue getIssueById(Long id) {
		return quizRepo.findIssueById(id);		
	}	
	
	@Transactional
	public Quiz addQuiz(Quiz quiz) {
		return quizRepo.save(quiz);
	}

	@Transactional
	public Issue saveIssue(Issue issue) {
		return issueRepo.save(issue);
	}
	
	@Transactional
	public Issue addQuizIssue(Issue issue) {
		Long idOfNewIssue =  quizRepo.getCountOfIssuesInQuiz(issue.getFkIdQuiz());		
		issue.setIdIssue(++idOfNewIssue);
		return issueRepo.save(issue);
	}

	@Transactional
	public Answer addAnswer(Answer answer) throws RestConflictException {
		if (quizRepo.getCountFinalAnswers(answer.getFkIdIssue()) > 0) {
			throw new RestConflictException("Final answer already exists for fkIdIssue()="+answer.getFkIdIssue());
		}
		return answerRepo.save(answer);
	}		
	
}
