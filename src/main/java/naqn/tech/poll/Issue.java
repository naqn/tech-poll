package naqn.tech.poll;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "issue")
public class Issue {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "issue_generator")
    @SequenceGenerator(name="issue_generator", sequenceName = "issue_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)       
	private Long id;
	private Long idIssue;	
	private Long fkIdQuiz;	
	
	@NotBlank(message = "Question is mandatory")
    private String question;
	@NotBlank(message = "A option is mandatory")	
    private String a;    
	@NotBlank(message = "B option is mandatory")	
    private String b;
	@NotBlank(message = "C option is mandatory")	
    private String c;
	@NotBlank(message = "D option is mandatory")	
    private String d;
	
	public Issue() {
	}
	
	public Issue(String question, String a,  String b, String c,  String d) {
		super();
		this.question = question;
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFkIdQuiz() {
		return this.fkIdQuiz;
	}
	public void setFkIdQuiz(Long fkIdQuiz) {
		this.fkIdQuiz = fkIdQuiz;
	}
	public Long getIdIssue() {
		return this.idIssue;
	}
	public void setIdIssue(Long idIssue) {
		this.idIssue = idIssue;
	}

	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getB() {
		return b;
	}
	public void setB(String b) {
		this.b = b;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public String getD() {
		return d;
	}
	public void setD(String d) {
		this.d = d;
	}
		
}
