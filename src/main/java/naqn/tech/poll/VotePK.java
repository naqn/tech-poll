package naqn.tech.poll;

import java.io.Serializable;

public class VotePK implements Serializable {

	private static final long serialVersionUID = -7023328037929531319L;
	protected Long id;
    protected Long fkIdQuestion;

    public VotePK() {}

    public VotePK(Long idVote, Long fkIdQuestion) {
        this.id = idVote;
        this.fkIdQuestion = fkIdQuestion;
    }
    // equals, hashCode
}
