package naqn.tech.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class RestConflictException extends RuntimeException {

	private static final long serialVersionUID = 6910342040003345252L;	

	public RestConflictException(String string) {
		super(string);
	}

}
