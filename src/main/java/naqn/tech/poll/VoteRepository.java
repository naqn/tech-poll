package naqn.tech.poll;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface VoteRepository extends CrudRepository<Vote, String> {		

	@Query(value = "SELECT v FROM Vote v WHERE v.fkIdQuestion = :fkIdQuestion order by v.id desc")	
	List<Vote> findVotesForQuestion(@Param("fkIdQuestion") Long fkIdQuestion);

	@Query(value = "SELECT count(*) FROM Vote v WHERE v.fkIdQuestion = :idQuestion")	
	Long getCountOfVotesForQuestion(@Param("idQuestion") Long idQuestion);	
	
}
