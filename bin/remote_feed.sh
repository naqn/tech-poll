REMOTE_TARGET=$1
TIME=$(date '+%s%N')
curl $REMOTE_TARGET/quiz -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"id":"1","name":"POLSKA", "time":"'$TIME'"}'

TIME=$(date '+%s%N')
curl $REMOTE_TARGET/quiz -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"id":"2","name":"ANGLIA", "time":"'$TIME'"}'

TIME=$(date '+%s%N')
curl $REMOTE_TARGET/quiz -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"id":"3","name":"SŁOWACJA", "time":"'$TIME'"}'

curl $REMOTE_TARGET/quiz/1/issues -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"question":"Kto odkrył pierwiastek rad?","a":"a) Niels Henrik David Bohr", "b":"b) Maria Skłodowska-Curie","c":"c) Pierre Curie", "d":"d) odpowiedzi b) i c)","description":"Kto?"}'
curl $REMOTE_TARGET/quiz/1/issues -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"question":"Kiedy odbyła się bitwa pod Cedynią?","a":"a) 956 n.e.", "b":"b) 14 kwietnia 966 n.e.","c":"c) 24 czerwca 972 n.e.", "d":"d) 23 kwietnia 997 n.e.","description":"Kiedy?"}'
curl $REMOTE_TARGET/quiz/1/issues -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"question":"Co jest atrybutem Świętego Dominika (Guzmána) ?","a":"a) gwiazda nad głową, pies z pochodnią w pysku", "b":"b) cyborium i figura Matki Boskiej","c":"c) słońce na piersiach", "d":"d) ptaki, baranek, ryba w ręku","description":"Co?"}'

curl $REMOTE_TARGET/quiz/2/issues -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"question":"Kto?","a":"A","b":"B","c":"C","d":"D","description":"kto-kto"}'
curl $REMOTE_TARGET/quiz/2/issues -X POST -H "Content-Type: application/json; charset=utf-8" -d '{"question":"Kiedy?","a":"A","b":"B","c":"C","d":"D","description":"kiedy-kiedy"}'

