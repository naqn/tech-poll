package naqn.tech.poll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuizController {

	@Autowired
	private QuizService quizService; 
	
	@RequestMapping("/quiz")
	public List<Quiz> getAllWastes() {
		return quizService.getAllQuizes();
	}

	@RequestMapping(method=RequestMethod.GET, value="/quiz/{idQuiz}")
	public Quiz getQuiz(@PathVariable (name = "idQuiz") Long idFromUrl) {
		return quizService.getQuiz(idFromUrl);
	}

	@RequestMapping(method=RequestMethod.GET, value="/quiz/{idQuiz}/issues")
	public List<Issue> getQuizIssues(@PathVariable (name = "idQuiz") Long idQuiz) {
		return quizService.getQuizIssues(idQuiz);
	}

	@RequestMapping(method=RequestMethod.GET, value="/quiz/{idQuiz}/issues/{idIssue}")
	public Issue getIssueOfQuiz(@PathVariable (name = "idQuiz") Long idQuiz, @PathVariable (name = "idIssue") Long idIssue) {
		return quizService.getIssueOfQuiz(idQuiz, idIssue);
	}	
	
	@RequestMapping(method=RequestMethod.POST, value="/quiz/{idQuiz}/issues")
	public Issue addQuizIssue(@PathVariable (name = "idQuiz") Long idQuiz, @RequestBody Issue issue) {		
		issue.setFkIdQuiz(idQuiz);
		return quizService.addQuizIssue(issue);
	}	
	
	@RequestMapping(method=RequestMethod.POST, value="/quiz")
	public Quiz addQuiz(@RequestBody Quiz quiz) {
		return quizService.addQuiz(quiz);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/answers")
	public Answer addAnswer(@RequestBody Answer answer) {
		answer.setTime(""+System.currentTimeMillis());		
		return quizService.addAnswer(answer);
	}
	
	
}
